<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

class Validaciones {

  function validarNumeroCasos($t){
		//Validamos que el numero de casos a realizar este entre 0 y 50
		if($t<1 || $t>50){
			return false;
		}else{
			return true;
		}
	}

	function validarTamanoMat($n){
		//Validamos que el tamaño de la matriz (n) este entre 0 y 100
		if($n<1|| $n>100){
			return false;
		}else{
			return true;
		}
	}

	function validarNumeroQuery($m){
		//Validamos que el numero de querys(m) este entre 0 y 1000
		if($m<1 || $m>1000){
			return false;
		}else{
			return true;
		}
	}

	function validarUpdate($tamano, $x, $y, $z){
		//Validamos que el numero de querys(m) este entre 0 y 1000
		if($x>$tamano || $y>$tamano || $z>$tamano){
			return false;
		}else{
			return true;
		}
	}

	function validarNroCasosTipo($arrayCasos, $nroquerys){
		$nroCasos=count($arrayCasos);
		if($nroCasos==$nroquerys)
			return true;
		else
			return false;
	}

	function validarExpresion($expresion, $nroParametrosEsperados){
		if(count($expresion)==$nroParametrosEsperados){
			return true;
		}else{
			return false;
		}
	}
}

?>