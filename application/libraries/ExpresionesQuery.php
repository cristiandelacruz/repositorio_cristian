<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

class ExpresionesQuery {

	function evaluarExpresion($expresion, $matriz){
  	//Obtenemos instancia necesaria par cargar vistas
		$CI = &get_instance();

	//Cargamos las librerias
		$CI->load->library('validaciones');
		$CI->load->library('operacionesmatriz');
		$CI->load->helper('url');

  	//Separamos las expresiones por espacio
		$arrayExpresion=preg_split('/\s+/', $expresion);
  	//Identificamos si la palabra es query o update
		if($arrayExpresion[0]=='query'){
  		//Verificamos el numero de parametros del query
			if($CI->validaciones->validarExpresion($arrayExpresion, 7)){
				$x1=$arrayExpresion[1];
				$y1=$arrayExpresion[2];
				$z1=$arrayExpresion[3];
				$x2=$arrayExpresion[4];
				$y2=$arrayExpresion[5];
				$z2=$arrayExpresion[6];
				//Ejecutamos la consulta
				$suma= $CI->operacionesmatriz->query($matriz, $x1, $y1, $z1, $x2, $y2, $z2);
				$datos = array('suma' => $suma, 'operacion'=>'query '.$x1.' '.$y1.' '.$z1.' '.$x2.' '.$y2.' '.$z2); 
				$CI->load->view('resultado', $datos);
				
			}else{
				$datos = array('error' => 'Error de sintaxis en query, numero de parametros diferente'); 
				$CI->load->view('error', $datos);
			}

		}elseif ($arrayExpresion[0]=='update') {
			$x=$arrayExpresion[1];
			$y=$arrayExpresion[2];
			$z=$arrayExpresion[3];
			$w=$arrayExpresion[4];
  			//Verificamos el numero de parametros del query
			if($CI->validaciones->validarExpresion($arrayExpresion, 5)){
				$matriz=$CI->operacionesmatriz->update($matriz, $x, $y, $z, $w);
			}else{
				$datos = array('error' => 'Error de sintaxis en update, numero de parametros diferente'); 
				$CI->load->view('error', $datos);
			}
		}else{
			$datos = array('error' => 'No se reconoce palabra '.$arrayExpresion[0].' (palabras validas: "query" y "update")'); 
			$CI->load->view('error', $datos);
		}
		return $matriz;
	}

  //Tomar como base para evaluar la expresion el numero de caracteres

  //Automata para evaluar query

  //Automata para evaluar update
}

?>