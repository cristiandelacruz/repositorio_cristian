<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

class OperacionesMatriz {
   //funciones que queremos implementar en Miclase.


	function inicializar($tamano){
		//Recorremos la matriz para inicializarla en cero
		
		for($i=0; $i< $tamano; $i++){
			for($j=0; $j<$tamano; $j++){
				for($k=0; $k<$tamano; $k++){
					$matriz[$i][$j][$k]=0;
				}		
			}	
		}
		return $matriz;
	}

	function update($matriz, $x, $y, $z, $w){
		//Actualiza la matriz en las cordenadas x, y, z con el valor w
		$matriz[$x][$y][$z]=$w;
		return $matriz;
	}

	function query($matriz, $x1, $y1, $z1, $x2, $y2, $z2){
		$suma=0;
		for($i=$x1; $i<= $x2; $i++){
			for($j=$y1; $j<=$y2; $j++){
				for($k=$z1; $k<=$z2; $k++){
					$suma+=$matriz[$i][$j][$k];
				}		
			}	
		}
		return $suma;
	}

	function ejecutarCasos($nro_casos){
		 $CI = &get_instance();
		for($i=0; $i< $nro_casos; $i++){
			$CI->load->helper('form');
			$CI->load->view("configuracion");
		}
	}
}

?>