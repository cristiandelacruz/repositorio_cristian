<?php
class matriz extends CI_Controller {
	

	function index()
	{
   		$this->load->helper('form');
    	$this->load->view("inicio");
	}
	
	function numero_casos(){
		//obtenemos los valores
	        $nro_caso = $this->input->post('nro_casos');

	        //Cargamos las librerias
	        $this->load->library('validaciones');
	        $this->load->library('operacionesmatriz');
	        $this->load->helper('url');

	        //Validamos el numero de casos
	        if(! ($this->validaciones->validarNumeroCasos($nro_caso))){
	        	$datos = array('error' => 'Numero de casos no valido (Debe estar entre 1 y 50)'); 
	        	$this->load->view('error', $datos);
	        	return;
	        }

	        //Iniciamos los casos
	        $this->operacionesmatriz->ejecutarCasos($nro_caso);

	}

	function ingresar_datos(){
			//obtenemos los valores
	        $tamquery = $this->input->post('tamanoquery');
	        $tquery = $this->input->post('t_query');

	        //Cargamos las librerias
	        $this->load->library('operacionesmatriz');
	        $this->load->library('validaciones');
	        $this->load->library('expresionesquery');
        
	        //separamos el valor del tamaño matriz y numero de queris
			$arrayValores=preg_split('/\s+/', $tamquery);
			$tamanoMatriz=$arrayValores[0];
			$numeroCasos=$arrayValores[1];

			//Validamos el tamaño del matriz
			if(! ($this->validaciones->validarTamanoMat($tamanoMatriz))){
	        	$datos = array('error' => 'Tamaño de matriz invalido'); 
	        	$this->load->view('error', $datos);
	        	return;
	        }

	        //Validamos el numero de casos a realizar
	        if(! ($this->validaciones->validarTamanoMat($numeroCasos))){
	        	$datos = array('error' => 'Numero de querys a ejecutar invalido'); 
	        	$this->load->view('error', $datos);
	        	return;
	        }

	        //Inicializamos la matriz
	        $matriz=$this->operacionesmatriz->inicializar($tamquery);

	        //obtenemos cada uno de los queries
	        $arrayquerys = explode("\n", $tquery);
	        
	        //Validar que el numero de casos es igual al ingresado
	        if($this->validaciones->validarNroCasosTipo($arrayquerys,$numeroCasos)){
	        	//Realizamos un ciclo y para evaluar cada una de las exprexiones del query
		        for($i=0; $i< $numeroCasos; $i++){
		        	$matriz=$this->expresionesquery->evaluarExpresion(trim($arrayquerys[$i]), $matriz);
		        }
	        }else{
				$this->load->helper('url');
	        	$datos = array('error' => 'El numero de queries no es igual al total de los querys digitados <br> Nota: Verifique no existe salto de linea al final'); 
	        	$this->load->view('error', $datos);
	        	return;
	        }

	        //Mostramos la salida de resultados  
	}

}
?>